const express = require('express');
const morgan = require('morgan');
const api = require('./routes/api');
const app = express();
const path = require('path');
const port = 3000;

app.use(morgan('combined'));
app.set('view engine', 'ejs');
app.use('/api', api);;

app.use(express.static(__dirname + '/public'))

app.get('/', function(req, res){
  const name = req.query.name || 'Home'
  res.render(path.join(__dirname, './views/home'),{
    name : name
  });
});

app.get('/play', function(req, res){
  res.render(path.join(__dirname, './views/game'));
})

app.get('/check-log', function(req, res){
  res.json({
    'check' : 'log'
  });
});

app.use(function(err, req, res, next){
  res.status(500).json({
    status: 'fail',
    errors: err.message
  })
});

app.use(function(req, res, next){
  res.status(404).json({
    status: 'NOT FOUND',
    errors: 'File Not Found'
  })
})

app.listen(port, () => { console.log("Server Berhasil dijalankan"); });