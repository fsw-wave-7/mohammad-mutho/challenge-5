const express = require('express');
const { json } = require('body-parser');

const Post = require('../controller/post.js');
const post = new Post;

const logger = (req, res, next) => {
  console.log(`LOG: ${req.method} ${req.url}`);
  next();
}

const api = express.Router(); // lebih di spesifikasikan call function router aja

// api.use(logger);
api.use(json());

api.get('/post', post.getPost);
api.get('/post/:index', post.getDetailPost);
api.post('/post', post.insertPost);
api.put('/post/:index', post.updatePost);
api.delete('/post/:index', post.deletePost);

module.exports = api